# flask-chat.py

## A simple web-based chatting, using flask, python, angular.js

using server-send-events technology, so it can be used as real-time communication between server and client

requirements:
```
pip install flask
```
to run:
```
python flask-chat.py
```