from flask import Flask, request, Response, stream_with_context, jsonify
from Queue import Queue
import json

app = Flask(__name__)

@app.route('/')
def home():
	return """
	<html lang="en" ng-app="myApp">
	<head>
		<title>Chat App</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
		<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
		<script>
			var myApp = angular.module('myApp', []);
			myApp.controller('chatCtrl', function($scope, $http) {
				$scope.chats = [];
				$scope.send = { "user": "", "chat": "" };
				$scope.isUserSet = false;
				var evtSrc = new EventSource('/subscribe/chat');
				if (evtSrc) {
					evtSrc.addEventListener('chat', function(e) {
						console.log(e.data);
						var line = JSON.parse(e.data);
						$scope.$apply(function() {
							$scope.chats.push(line)
						});
					});
					$scope.$on('$locationChangeSuccess', function(event) {
						evtSrc.close();
					});
				}
				$scope.doSend = function() {
					if ($scope.send.text !== '') {
						console.log($scope.send);
						$http.post('/publish/chat', $scope.send)
							.then(function (response) {
								console.log(response);
								$scope.send.chat = '';
								$scope.isUserSet = true;
							});
					}
				}
			});
		</script>
	</head>
	<body>
		<div class="container">
			<h1>Welcome to Chat App</h1>
			<div class="row" ng-controller="chatCtrl">
				<p ng-repeat="line in chats">
					{{ line.user }} : {{ line.chat }}
				</p>
				<form class="form-inline" ng-submit="doSend()">
					<div class="form-group" ng-hide="isUserSet">
						<input type="text" class="form-control" placeholder="User name" ng-model="send.user" />
					</div>
					<div class="form-group">
						<label ng-show="isUserSet">{{send.user}}</label>
						<input type="text" class="form-control" placeholder="Text" ng-model="send.chat" />
					</div>
					<button class="btn btn-primary">Send</button>
				</form>
			</div>
		</div>
	</body>
	</html>
	"""

channels = {}

@app.route('/subscribe/<channel>')
def subscribe(channel):
	def generator():
		if channel not in channels:
			channels[channel] = []
		q = Queue()
		channels[channel].append(q)
		try:
			while True:
				data = q.get()
				yield "\n\nevent: {event}\ndata: {data}\n\n".format(event=channel, data=json.dumps(data))
		finally:
			channels[channel].remove(q)
	return Response(stream_with_context(generator()), mimetype='text/event-stream')

@app.route('/publish/chat', methods=['GET', 'POST'])
def publish():
	data = request.get_json()
	if not data:
		data = { 'user': request.values['user'], 'chat': request.values['chat'] }
	if 'chat' in channels:
		for q in channels['chat'][:]:
			q.put(data)
		return jsonify(success=True, data=data)
	return jsonify(success=False, data=data)

@app.route('/debug')
def debug():	
	print "Hello"
	return "<br/>".join(
		"{count} subscription to channel {channel}".format(
		channel=channel, count=len(subscribers)) for channel, subscribers in channels.iteritems())

if __name__ == "__main__":
	app.debug = True
	app.run(host='0.0.0.0', threaded=True)